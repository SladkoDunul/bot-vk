import time
import vk_api
import traceback
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id
from bot_config import TOKEN
import requests

session = requests.Session()
vk_session = vk_api.VkApi(token=TOKEN)

vk = vk_session.get_api()

longpoll = VkBotLongPoll(vk_session,189937888)

keyboard = VkKeyboard()
keyboard.add_button('Новости', color=VkKeyboardColor.PRIMARY)
keyboard.add_button('Мероприятия', color=VkKeyboardColor.POSITIVE)


attachments = []
from vk_api import VkUpload 
upload = VkUpload(vk_session)
image_url = 'https://cdn.discordapp.com/attachments/564841628070576139/658391132514025486/-1.jpg'
image = session.get(image_url, stream=True)
photo = upload.photo_messages(photos=image.raw)[0]
attachments.append(
    'photo{}_{}'.format(photo['owner_id'], photo['id'])
)
vk.messages.send(
    user_id=495915920,
    # attachment=','.join(attachments),
    random_id=get_random_id(),
    keyboard=keyboard.get_keyboard(),
    message='Привет дружок'
)



print('Бот работает')






def keyboard_vk(event,message):
    vk.messages.send(user_id=495915920,random_id=get_random_id(),keyboard=keyboard.get_keyboard(),message=message)

def keyboard_vk_cls(event,message):
    vk.messages.send(user_id=495915920,random_id=get_random_id(),keyboard=keyboard.get_empty_keyboard(),message=message)


def send_messages(event,message):
    vk.messages.send(user_id=event.obj.message['from_id'],random_id=get_random_id(),message=message)

def main():
    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            ''' Проверям что пришло сообщение от пользователя и отправляем в ответ '''
            if event.from_user:
                text = event.obj.message['text']
                print(f'Сообщение: {text}')
                if event.obj.message['text'] == 'Новости':
                    vk.messages.send(user_id=495915920,attachment=','.join(attachments),random_id=get_random_id(),message='ФотОчка')
                    keyboard.get_empty_keyboard()
                    keyboard.add_button('Новость 1', color=VkKeyboardColor.PRIMARY)
                    keyboard.add_button('новость 2', color=VkKeyboardColor.POSITIVE)
                    keyboard_vk(event,message='Новости')
                elif event.obj.message['text'] == 'Мероприятия':
                    send_messages(event,message='https://topstudents.ru/archives/event')
                else:
                    send_messages(event,message='ПНХ')


if __name__ == '__main__':
    main()
